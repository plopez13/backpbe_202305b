const verificarGet = require("../middleware/verificarGet");
const verificarToken = require("../middleware/verificarToken");

module.exports = function (app) {
   app.get(
    "/productosPorCategoria/:id",
    verificarGet.verificarNumero,
    verificarToken.admin,
    async function (req, res) {
      const id = req.params.id;

      const productos = require("../services/productosCategoriaServices.js");

      const response = await productos.getProductsByCategory(id);

      res.send(response);
    }
  );

  app.get(
    "/productosPorCategoria/:id/:price",
    verificarGet.verificarNumero,
    verificarToken.admin,
    async function (req, res) {
      const id = req.params.id;
      const price = parseFloat(req.params.price);

      const productos = require("../services/productosCategoriaServices.js");

      const response = await productos.getProductsByCategoryAndPrice(id, price);

      res.send(response);
    }
  );

};
