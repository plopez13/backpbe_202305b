const verificarToken = require("../middleware/verificarToken");
const verificarGet = require("../middleware/verificarGet");

module.exports = function (app) {
  app.get("/categoria", verificarToken.verificar, async function (req, res) {
    const categorias = require("./../services/categoriasServices.js");
    // se coloca ./ y ../ para subir un nver en carpeta y entrar en service
    const response = await categorias.getCategorias();

    res.send(response);
  });

  app.get(
    "/categoria/:id",
    verificarGet.verificarNumero,
    verificarToken.verificar,
    async function (req, res) {
      const id = req.params.id;

      const categoria = require("./../services/categoriasServices.js");

      const response = await categoria.getCategoriasById(id);

      res.send(response);
    }
  );

  app.post("/categoria",verificarToken.verificar, async function (req, res) {
    const categoriaNueva = req.body;

    const categoria = require("./../services/categoriasServices.js");

    const response = await categoria.postCategoria(categoriaNueva);

    res.send(response);
  });
  app.put("/categoria", async function (req, res) {
    const categoriaAct = req.body;

    const categoria = require("./../services/categoriasServices.js");

    const response = await categoria.putCategoria(categoriaAct);

    res.send(response);
  });
};
