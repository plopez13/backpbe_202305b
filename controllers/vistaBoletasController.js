const verificarGet = require("../middleware/verificarGet");
const verificarToken = require("../middleware/verificarToken");

module.exports = function (app) {
  app.get(
    "/vistaBoletas",
    verificarToken.verificar,
    verificarToken.admin,
    async function (req, res) {
      const vistaBoletas = require("./../services/vistaBoletasSevices");

      const response = await vistaBoletas.getVistaBoletas();

      
      res.send(response);
    }
  );
    app.get(
      "/vistaBoletasByPrice/:precio",
      verificarToken.verificar,
      verificarToken.admin,
      async function (req, res) {
  
        const precio = req.params.precio
        const vistaBoletas = require("./../services/vistaBoletasSevices");
  
        const response = await vistaBoletas.getVistaBoletasByPrice(precio);
  
        
        res.send(response);
      }
    );

  app.get(
    "/cliente/:id",
    verificarGet.verificarNumero,
    async function (req, res) {
      const id = req.params.id;

      const clientes = require("./../services/clientesServices.js");

      const response = await clientes.getClienteById(id);

      res.send(response);
    }
  );

  app.post("/clientes", async function (req, res) {
    const clienteNuevo = req.body;

    const clientes = require("./../services/clientesServices.js");

    const response = await clientes.postCliente(clienteNuevo);

    res.send(response);
  });

  app.put("/clientes", async function (req, res) {
    const clienteNuevo = req.body;

    const clientes = require("./../services/clientesServices.js");

    const response = await clientes.putCliente(clienteNuevo);

    res.send(response);
  });
};
