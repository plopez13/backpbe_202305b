const verificarToken = require("../middleware/verificarToken");
//const verificarGet = require("../middleware/verificarGet");

module.exports = function (app){

    app.get("/productos", async function(req, res){
        const productos = require("./../services/productosServices.js");
        const response = await productos.getProductos();
        res.send(response);
    });

    //get productos de baja
    app.get("/productos/baja",async function(req, res){
        const productos = require("./../services/productosServices.js");
        const response = await productos.getProductosDeBaja();
        res.send(response);
    });

    app.post("/productos",verificarToken.verificar ,async function (req, res) {
        const productoNuevo = req.body;
        const producto = require("./../services/productosServices.js");
        const response = await producto.postProductos(productoNuevo);
        res.send(response);

    });

    app.put("/productos", verificarToken.verificar, async function (req, res) {
        const productAct = req.body;
        const producto = require("./../services/productosServices.js");
        const response = await producto.putProductos(productAct);
        res.send(response);

    });
    app.delete("/productos", verificarToken.verificar, async function(req, res){
        const productBaja = req.body;
        const producto = require("./../services/productosServices");
        const response = await producto.deleteProductos(productBaja);
        res.send(response);

    });
    app.patch("/productos", verificarToken.verificar, async function (req, res) {
        const productAlta = req.body;
        const producto = require("./../services/productosServices.js");
        const response = await producto.altaProducto(productAlta);
        res.send(response);
    });
};