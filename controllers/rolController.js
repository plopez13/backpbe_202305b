const verificarToken = require('../middleware/verificarToken')
const verificarGet = require("../middleware/verificarGet")

module.exports = function (app) {
    app.get("/rol", async function (req, res) {
        //requerimos y guardamos la ruta de services donde hara la consulta a la base
        const roles = require("./../services/rolServices")
        const response = await roles.getRol()
        //Una vez recibida la respuesta, se la mandamos a la ruta
        res.send(response.result)
    })

    app.get("/rol/:id", verificarGet.verificarNumero, async function (req, res) {
        // tomamos el parametro de la ruta
        const id = req.params.id
        const roles = require("./../services/rolServices")
        const response = await roles.getRolById(id)
        if (response.error) {
            res.send(response.error)
        } else {
            res.send(response.result)
        }
    })
    app.post("/rol", async function (req, res) {

        const rolNuevo = req.body

        const roles = require("./../services/rolServices.js")

        const response = await roles.postRol(rolNuevo)

        res.send(response)
    })

    app.put("/rol", async function (req, res) {

        const rolNuevo = req.body

        const roles = require("./../services/rolServices.js")

        const response = await roles.putRol(rolNuevo)

        res.send(response)
    })
}
