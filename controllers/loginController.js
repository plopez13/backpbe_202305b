module.exports = function(app){

    app.post("/login", async function (req, res) {

        const bodyLogin = req.body

        const longinServices = require("./../services/loginServices.js")

        const response = await longinServices.postLogin(bodyLogin)

        res.send(response)
    })
}