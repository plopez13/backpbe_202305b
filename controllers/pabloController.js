module.exports = function(app){

    app.get("/pablo", async function (req, res) {
        const pablo = require("./../services/pabloServices.js")

        const response = await pablo.getPablo()

        res.send(response)
    })

}