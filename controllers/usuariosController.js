const verificarToken = require("../middleware/verificarToken");
const verificarGet = require("../middleware/verificarGet");

module.exports = function (app) {
  app.get("/usuarios",  async function (req, res) {
    const usuarios = require("./../services/usuariosServices.js");
    // se coloca ./ y ../ para subir un nivel en carpeta y entrar en service
    const response = await usuarios.getUsuarios();

    res.send(response);
  }),
    app.get(
      "/usuario/:id",
      verificarGet.verificarNumero,
      verificarToken.verificar,
      async function (req, res) {
        const id = req.params.id;

        // se coloca ./ y ../ para subir un nivel en carpeta y entrar en service
        const usuarios = require("./../services/usuariosServices.js");

        const response = await usuarios.getUsuariosById(id);

        res.send(response);
      }
    ),
    app.post("/usuarios", verificarToken.admin, async function (req, res) {
      const usuarioNuevo = req.body;

      const usuario = require("./../services/usuariosServices.js");

      const response = await usuario.postUsuarios(usuarioNuevo);

      res.send(response);
    }),
    app.put("/usuarios", verificarToken.admin, async function(req, res) {
     const usuarioNuevo = req.body;
     const usuario = require("./../services/usuariosServices.js");
     const response = await usuario.putUsuario(usuarioNuevo);
     res.send(response);
    });
    
};
