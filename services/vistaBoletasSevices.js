//requerimos el módulo para conectarse a la base de datos
const mysql = require("mysql");
//requerimos el archivo donde tenemos configurada la conexion
const conn = require("../config/conn");

//creamos la constante a ser exportada

const vistaBoletas = {
  async getVistaBoletas(precio) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT clientes.nombre,clientes.email, clientes.direccion, clientes.telefono, ventas.id, ventas.fecha, ventas.total FROM clientes INNER JOIN ventas ON clientes.id = ventas.cliente_id WHERE ventas.total <" + precio + ";";
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async getVistaBoletasByPrice(precio) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT clientes.nombre,clientes.email, clientes.direccion, clientes.telefono, ventas.id, ventas.fecha, ventas.total FROM clientes INNER JOIN ventas ON clientes.id = ventas.cliente_id WHERE ventas.total >" + precio + ";";
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async getVistaBoletasById(id) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM clientes WHERE id = " + id;
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },


};

module.exports = vistaBoletas;
