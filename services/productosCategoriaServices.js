//requerimos el módulo para conectarse a la base de datos
const mysql = require("mysql");
//requerimos el archivo donde tenemos configurada la conexion
const conn = require("../config/conn");

//creamos la constante a ser exportada

const productos = {
  async getProductsByCategory(id) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT p.* FROM productos p INNER JOIN categorias c ON c.id = p.categoria_id  WHERE c.id = " + id;
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async getProductsByCategoryAndPrice(id, price) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT p.* FROM productos p INNER JOIN categorias c ON c.id = p.categoria_id  WHERE c.id = " + id + " AND p.precio >= " + price;
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      console.log(resultado.code)
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
 
};

module.exports = productos;
