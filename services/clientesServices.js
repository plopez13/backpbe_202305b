//requerimos el módulo para conectarse a la base de datos
const mysql = require("mysql");
//requerimos el archivo donde tenemos configurada la conexion
const conn = require("../config/conn");

//creamos la constante a ser exportada

const clientes = {
  async getClientes() {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT clientes.nombre,clientes.email, clientes.direccion, clientes.telefono, ventas.id, ventas.fecha, ventas.total FROM clientes INNER JOIN ventas ON clientes.id = ventas.cliente_id;";
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async getClienteById(id) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM clientes WHERE id = " + id;
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async postCliente(nuevo) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      "INSERT INTO `ecommerce`.`clientes` ( `id` ,`nombre` , `email`, `direccion` ,`telefono`)VALUES (NULL , '" +
      nuevo.nombre +
      "', '" +
      nuevo.email +
      "','" +
      nuevo.direccion +
      "', '" +
      nuevo.telefono +
      "');";
    console.log(sql);
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "Cliente agregado correctamente." };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async putCliente(nuevo) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      'UPDATE `clientes` SET `nombre`="' +
      nuevo.nombre +
      '",`email`="' +
      nuevo.email +
      '",`direccion`="' +
      nuevo.direccion +
      '",`telefono`="' +
      nuevo.telefono +
      '" WHERE `id` = ' +
      nuevo.id;

    console.log(sql);
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "Cliente actualizado correctamente." };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
};

module.exports = clientes;
