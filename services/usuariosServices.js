//requerimos el módulo para conectarse a la base de datos
const mysql = require("mysql");
//requerimos el archivo donde tenemos configurada la conexion
const conn = require("../config/conn");


//creamos la constante a ser exportada
const usuarios = {
  async getUsuarios() {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM usuarios";
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    //guardamos una respuesta de error por si no encuentra regristros
    let response = { mensaje: "No se encontraron registros" };
    if (resultado.code) {
      //si nos devuleve un codigo de error mostramos mensaje
      response = { mensaje: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      //se devuelve el resultaqdo al comprobar que la respuesta contiene datos
      response = { result: resultado };
    }
    return response;
  },

  async getUsuariosById(id) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM usuarios WHERE id = " + id;
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    //guardamos una respuesta de error por si no encuentra regristros
    let response = { mensaje: "No se encontraron registros" };
    if (resultado.code) {
      //si nos devuleve un codigo de error mostramos mensaje
      response = { mensaje: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      //se devuelve el resultaqdo al comprobar que la respuesta contiene datos
      response = { result: resultado };
    }
    return response;
  },

  async postUsuarios(nuevo) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      "INSERT INTO `ecommerce`.`usuarios`(`id`, `nombre`, `email`, `password`, `rol`)VALUES (NULL , '" +
      nuevo.nombre +
      "', '" +
      nuevo.email +
      "','" +
      nuevo.password +
      "', '" +
      nuevo.rol +
      "');";

    console.log(sql);
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { mensaje: "Usuario agregado correctamente." };
    if (resultado.code) {
        if(resultado.errno == 1062){
            response = { mensaje: "Existe una cuenta con el email Ingresado" };

        }else{
            response = { mensaje: "Error en la consulta SQL" };
        }
     
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async putUsuario(actualizaUsr){
    //se crea la consula sql para actualizar al usuario
    let sql = 'UPDATE `usuarios` SET `nombre`="' +  actualizaUsr.nombre + 
    '", `email`="' + actualizaUsr.email +
    '",`password`="' + actualizaUsr.password + 
    '", `rol`="' + actualizaUsr.rol +
    '" WHERE `id` = ' + actualizaUsr.id;
    console.log(sql);

    //con el archivo de la conexion se envia la consulta

    //utilizamos await porque se deconoce el tiempo de respuesta
    
    let resultado = await conn.query(sql);
    let response = { mensaje: "Usuario Actualizado correctamente"};

    if (resultado.code){
      console.log(resultado.code);
      response = {mensaje: "Error en la consulta"};
    }else if(resultado.length>0){
      response = {result: resultado}
    }
    return response;
  }
};
module.exports = usuarios;
