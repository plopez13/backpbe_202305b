//requerimos el módulo para conectarse a la base de datos
const mysql = require("mysql");
//requerimos el archivo donde tenemos configurada la conexion
const conn = require("../config/conn");

const productos = {
  async getProductos() {
    //hacemos la consulta y guardamos el resultado
    let sql = "SELECT * FROM `productos` WHERE `activo` = 1";
    //se guarda la consulta en resultado con la funcion await para esperar la respuesta
    //luego trabajar con ese resultado
    let resultado = await conn.query(sql);

    //se guarda mensaje de respuesta
    let response = { mensaje: "No se encontraron registros" };

    if (resultado.code) {
      response = { mensaje: "Error en consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async getProductosDeBaja(){
    let sql = "SELECT * FROM `productos` WHERE `activo` = 0";
    let resultado = await conn.query(sql);
    let response = {mensaje: "No se encontraron productos eliminados"};

    if(response.code){
        response = {mensaje: "Error en consulta SQL"};
    }else if(resultado.length > 0){
        response = {result: resultado};
    }
    return response;
  },

  async postProductos(nuevo) {
    let sql =
      "INSERT INTO `productos`(`id`, `nombre`, `precio`, `stock`, `categoria_id`, `activo`)VALUES (NULL,'" +
      nuevo.nombre +
      "',  '" +
      nuevo.precio +
      "', '" +
      nuevo.stock +
      "', '" +
      nuevo.categoria_id +
      "', '" +
      nuevo.activo +
      "');";

   console.log(sql);

    let resultado = await conn.query(sql);
    let response = { mensaje: "Producto agregado correctamente" };

    if (response.code) {
      response = { mensaje: "Error en la consulta SQL" };
      console.log("Error");
    } else if (resultado.length > 0) {
      response = { result: resultado };
      console.log("deberia guardar");
    }
    return response;
  },

  async putProductos(actualizaProd) {
    //al actualizar producto no se modifica si esta activo o no
    let sql =
      "UPDATE `productos` SET `nombre`='" +
      actualizaProd.nombre +
      "',`precio`='" +
      actualizaProd.precio +
      "',`stock`='" +
      actualizaProd.stock +
      "',`categoria_id`='" +
      actualizaProd.categoria_id +
      "' WHERE id =" +
      actualizaProd.id;
    //console.log(sql);

    let resultado = await conn.query(sql);
    let response = { mensaje: "Producto actualizado correctamente" };

    if (resultado.code) {
      response = { mensaje: "Error en consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async deleteProductos(actProd) {
    let sql =
      "UPDATE `productos` SET `activo`='" +
      actProd.activo +
      "' WHERE `id` =" +
      actProd.id;
    let resultado = await conn.query(sql);
    let response = {mensaje:"Se dio de baja el producto"};
    
    if(resultado.code){
        response = { mensaje: "Error en consulta SQL"};
    }else if(resultado.length > 0){
        response = { result: resultado};
    }
    return response;
  },

  async altaProducto(prod){
    let sql = "UPDATE `productos` SET `activo`='"+prod.activo+"' WHERE `id`=" +prod.id;
    let resultado = await conn.query(sql);
    let response = {mensaje: "se dio de alta el producto"};

    if(resultado.code){
        response = {mensaje: "Error en la consula SQL"};
    }else if(resultado.length > 0){
        response = {result: resultado};
    }
    return response;
  }
};
module.exports = productos;
