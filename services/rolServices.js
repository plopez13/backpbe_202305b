const mysql = require('mysql')
const conn = require('../config/conn')


const roles = {
    async getRol (){
        let sql = 'SELECT * FROM rol'
        let resultado = await conn.query(sql)
        let response = {error: "No se encontraron registros"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },
    async getRolById (id){
       
        let sql = 'SELECT * FROM rol WHERE id =' + id
        let resultado = await conn.query(sql)
        let response = {error: "No se encontraron registros"}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },

    async postRol (nuevo){
        let sql = "INSERT INTO `ecommerce`.`rol` ( `id` ,`nombre`) VALUES ('"+ nuevo.id + "', '"+ nuevo.nombre + "');"
        console.log(sql)
        let resultado = await conn.query(sql)
        let response = {result: "Rol agregado correctamente."}
        if(resultado.code) {
            response = {error: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    },

    async putRol (nuevo){
        let sql =  'UPDATE `rol` SET `nombre`= "' + nuevo.nombre + '" WHERE `id` = '+ nuevo.id 
        
        console.log(sql)
        
        let resultado = await conn.query(sql)
        let response = {result: "Rol actualizado correctamente."}
        if(resultado.code) {
            response = {result: "Error en la consulta SQL"}
        }else if (resultado.length > 0) {
            response = {result: resultado}
        }
        return response
    }


}

module.exports = roles