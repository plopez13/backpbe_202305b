//requerimos el módulo para conectarse a la base de datos
const mysql = require("mysql");
//requerimos el archivo donde tenemos configurada la conexion
const conn = require("../config/conn");

//se crea la  constante para ser exportada
const categorias = {
  async getCatagorias() {
    //se guarda la en la variable sql la consulta a base de datos
    let sql = "SELECT * FROM categorias";
    //se guarda en una variable el resultado de la consulta sql
    let resultado = await conn.query(sql);

    //guardamos una respuesta de error por si no encuentra regristros
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async getCategoriaById(id) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM categorias WHERE id = " + id;
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async postCategorias() {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM usuarios";
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);

    //guardamos una respuesta de error po si no encuentra regristros
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async postCategorias(nuevo) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      "INSERT INTO `ecommerce`.`categorias`(`id`, `nombre`)VALUES (NULL , '" +
      nuevo.nombre +
      "');";

    console.log(sql);
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "Categoria agregada correctamente." };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async putCategoria(nuevo) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      'UPDATE `categorias` SET `nombre`="' +
      nuevo.nombre +
      '" WHERE `id` = ' +
      nuevo.id;

    console.log(sql);
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "categoria actualizada correctamente." };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  /*async deleteCategoriaById(id) {
    let sql = "DELETE FROM `ecommerce`.`categorias` WHERE id = " + id;
    let resultado = await conn.query(sql);
    let response = { error: "Categoria eliminada correctamente" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },*/
};
module.exports = categorias;
