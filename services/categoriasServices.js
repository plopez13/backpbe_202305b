//requerimos el módulo para conectarse a la base de datos
const mysql = require("mysql");
//requerimos el archivo donde tenemos configurada la conexion
const conn = require("../config/conn");
//creamos la constante a ser exportada
const categorias = {
  async postCategoria(nuevo) {
    let sql =
      "INSERT INTO `categorias`(`id`, `nombre`) VALUES (NULL,'" +
      nuevo.nombre +
      "')";
    let resultado = await conn.query(sql);
    let response = { mensaje: "Categoria agregada correctamente" };

    if (resultado.code) {
        if(resultado.errno == 1062){
            response = { mensaje: "Error - Ya existe categoria" };
        }else{
            response = { mensaje: "Error en consulta SQL" };
        }
      
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  //dentro de ella ponemos una funcion asincrona, porque no sabemos cuanto demora la base en responder
  async getCategorias() {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM categorias";
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL"};
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async getCategoriasById(id) {
    let sql = "SELECT * FROM categorias WHERE id = " + id;
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async putCategoria(actCategoria) {
    let sql =
      "UPDATE `categorias` SET `nombre`='" +
      actCategoria.nombre +
      "' WHERE `id`=" +
      actCategoria.id;
    let resultado = await conn.query(sql);
    let response = { mensaje: "Se actulaizo categoria" };

    if (resultado.code) {
      response = { mensaje: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: respuesta };
    }
    return response;
  },
};
//Exportamos el módulo
module.exports = categorias;
