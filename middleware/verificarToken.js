//requerimos JWT
const jwt = require('jsonwebtoken')
//Tomamos la Clave secreta de las variables de entorno
const secret = process.env.SECRET;  

const verificarToken = {
    verificar: (req, res, next) => {
        try {
            const token = req.headers.authorization
            console.log("cargo token")

            const secret = process.env.SECRET

            const payload = jwt.verify(token,secret)
            console.log("verifico token")

            if (Date.now() > payload.exp) {
                return res.status(401).send({ error: "token expired" })
            }
            next();
        } catch (error) {
            res.status(401).send("hay un error al verificar token.")
            console.log(error)
        }
    },
    admin: (req, res, next) => {
            const token = req.headers.authorization
            const secret = process.env.SECRET
            const payload = jwt.verify(token, secret)

            if (payload.rol != 1) {
                return res.status(401).send({ error: "Usuario debe ser Admin" })
            }
            next();
    }
}

module.exports = verificarToken