// Requerimos express y aguardamos en una variable, que es un entorno de trabajo de node para poder trabajar con apis.
const express = require('express')
const app = express()
// body parser nos ayuda a deserializar el body de una request
const bodyParser = require('body-parser')

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

require("dotenv").config();

const secret = process.env.SECRET;

//generamos el servidor

require("./controllers/clientesController")(app);
require("./controllers/pabloController")(app);
require("./controllers/usuariosController")(app);
require("./controllers/categoriaController")(app);
require("./controllers/loginController")(app);
require("./controllers/ventasController")(app);
require("./controllers/productosController")(app);
require("./controllers/categoriaController")(app)
require("./controllers/ventasController")(app)
require("./controllers/rolController")(app)
require("./controllers/vistaBoletasController")(app)
require("./controllers/productosCategoriaController")(app)

app.listen(3000, function () {
    console.log("servidor iniciado en el puerto 3000");
})

